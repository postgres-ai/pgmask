FROM perl:5.32

WORKDIR /app

COPY app.pl /app
COPY cpanfile /app

RUN apt-get update && apt-get install -y git

RUN cpanm Module::Build

RUN git clone https://gitlab.com/depesz/Pg--Explain.git && \
    cd Pg--Explain && \
    perl Build.PL && \
    ./Build && \
    ./Build install

RUN cpanm --installdeps .

EXPOSE 3000

CMD ["perl", "app.pl"]

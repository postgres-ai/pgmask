#!/usr/bin/env perl
use Dancer2;
use Pg::Explain;

# Anonymize EXPLAIN query
post '/anonymize' => sub {
    my $data   = from_json(request->body);
    my $explain = $data->{explain};
    
    my $pg_explain = Pg::Explain->new({ explain => $explain });
    $pg_explain->anonymize();

    return to_json({ anonymized => $pg_explain->explain_string });
};

# Deanonymize EXPLAIN query
post '/deanonymize' => sub {
    my $data       = from_json(request->body);
    my $explain    = $data->{explain};
    my $deanon_map = $data->{deanon_map};

    my $pg_explain = Pg::Explain->new({ explain => $explain });
    $pg_explain->deanonymize($deanon_map);

    return to_json({ deanonymized => $pg_explain->explain_string });
};

start;
